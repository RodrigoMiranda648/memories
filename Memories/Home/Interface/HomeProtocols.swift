//
//  HomeProtocols.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 13/04/23.
//

import Foundation

protocol HomeViewModel: AnyObject{
    func dismissViews(completion: @escaping() -> Void)
}
