//
//  ViewController.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 26/03/23.
//

import UIKit

var memorie = [
    
    Content(description: "1a publicación", image: UIImage(systemName: "camera"), date: "11/10/2012"),
    Content(description: "2a publicación", image: UIImage(systemName: "camera.viewfinder"), date: "11/10/2012"),
    Content(description: "3a publicación", image: UIImage(systemName: "camera.viewfinder"), date: "11/10/2012")
]

private let homeText = "Home"
var isCellExpanded = true
class HomeViewController: UIViewController{
    
    //MARK: Properties
    private var viewModel: HomeViewModelImp?
    private var isAddMemorieButtonShown = true
    private let deltaFlag = 100.0
    private var deltaY = 0.0
    private var pastScrollValue = 0.0
    private let addButtonSize: CGFloat = 80
    
    //MARK: View Elements
    private let tableView: UITableView = {
        let table = UITableView()
        table.allowsSelection = false
        table.showsVerticalScrollIndicator = false
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    private let upperView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.systemOrange
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    private lazy var profileButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.backgroundColor = UIColor.systemBlue
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.clipsToBounds = true
        btn.layer.cornerRadius = 25
        btn.setImage(UIImage(systemName: "person.fill")?.withConfiguration(UIImage.SymbolConfiguration(pointSize: 35, weight: .regular)), for: .normal)
        btn.tintColor = UIColor.white
        btn.addTarget(self, action: #selector(profileButtonTapped), for: .touchUpInside)
        return btn
    }()
    private let homeLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = .systemFont(ofSize: 35)
        lbl.backgroundColor = .clear
        lbl.text = homeText
        lbl.clipsToBounds = true
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private lazy var addButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: view.frame.width - addButtonSize - 20, y: view.frame.height - addButtonSize - 50, width: 80, height: 80)
        btn.backgroundColor = UIColor.systemOrange
        btn.clipsToBounds = true
        btn.layer.cornerRadius = 40
        btn.contentHorizontalAlignment = .center
        btn.contentVerticalAlignment = .center
//        btn.setImage(UIImage(systemName: "plus"), for: .normal)
        btn.setImage(UIImage(systemName: "plus")?.withConfiguration(UIImage.SymbolConfiguration(pointSize: 35, weight: .bold)), for: .normal)
        btn.tintColor = .white
        btn.addTarget(self, action: #selector(addButtonTapped), for: .touchUpInside)
        return btn
    }()

    init(viewModel: HomeViewModelImp) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = .white
        viewModel?.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .clear
        tableView.register(CustomCell.self, forCellReuseIdentifier: "key")
        setupNextViewNavBar()
        setUpViewComponents()
        setUpGestureComponents()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK: Functions
    private func setUpViewComponents(){
        view.addSubview(upperView)
        view.addSubview(tableView)
        view.addSubview(profileButton)
        view.addSubview(homeLabel)
        view.addSubview(addButton)
        
        NSLayoutConstraint.activate([
            
            upperView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            upperView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            upperView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            upperView.heightAnchor.constraint(equalToConstant: 120),
            
            homeLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            homeLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            homeLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8),
            homeLabel.heightAnchor.constraint(equalToConstant: 30),
            
            profileButton.widthAnchor.constraint(equalToConstant: 50),
            profileButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            profileButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
            profileButton.heightAnchor.constraint(equalToConstant: 50),
            
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
            tableView.topAnchor.constraint(equalTo: homeLabel.bottomAnchor, constant: 20),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }
    private func setUpGestureComponents(){
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipeRightGesture.direction = .right
        view.addGestureRecognizer(swipeRightGesture)
    }
    func setupNextViewNavBar(){
        let backButton = UIBarButtonItem()
        backButton.title = "Regresar"

        navigationItem.backBarButtonItem = backButton
    }
    private func animateAddButton(){
        
        let translationAnimation = CABasicAnimation()
        translationAnimation.keyPath = "position.y"
        let scaleAnimation = CABasicAnimation()
        scaleAnimation.keyPath = "transform.scale"
        if isAddMemorieButtonShown{
            translationAnimation.fromValue = view.frame.height + addButtonSize/2
            translationAnimation.toValue = view.frame.height - addButtonSize/2 - 50
            scaleAnimation.fromValue = 0.1
            scaleAnimation.toValue = 1
        }else{
            translationAnimation.fromValue = view.frame.height - addButtonSize/2 - 50
            translationAnimation.toValue = view.frame.height + addButtonSize/2
            scaleAnimation.fromValue = 1
            scaleAnimation.toValue = 0.1
        }
//        translationAnimation.duration = 1
//        scaleAnimation.duration = 1
        let animationsGroup = CAAnimationGroup()
        animationsGroup.animations = [translationAnimation,scaleAnimation]
        animationsGroup.duration = 0.2
        addButton.layer.add(animationsGroup, forKey: nil)
        
        
        if isAddMemorieButtonShown{
            addButton.layer.position = CGPoint(x: view.frame.width - addButtonSize/2 - 20, y: view.frame.height - addButtonSize/2 - 50)
            addButton.layer.transform = CATransform3DMakeScale(1, 1, 1)
        }else{
            addButton.layer.position = CGPoint(x: view.frame.width - addButtonSize/2 - 20, y: view.frame.height + addButtonSize/2)
            addButton.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1)
        }
    }

    //MARK: Objc Functions
    @objc private func isCellExpandedButtonTapped(){
        isCellExpanded = !isCellExpanded
        tableView.reloadData()
    }
    @objc private func profileButtonTapped(){
        viewModel?.openLateralView(view: self)
    }
    @objc private func addButtonTapped(){
        viewModel?.openAddMemorie()
    }
    @objc private func handleSwipe(sender: UISwipeGestureRecognizer) {
        viewModel?.openLateralView(view: self)
    }
}

//MARK: Extensions
extension HomeViewController: UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return memorie.count == 0 ? 0 : memorie.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "key", for: indexPath) as! CustomCell
        
        cell.formatButton.addTarget(self, action: #selector(isCellExpandedButtonTapped), for: .touchUpInside)
        
        let model = memorie[indexPath.section]
        if isCellExpanded {
            cell.formatButton.setImage(UIImage(systemName: "arrow.down.right.and.arrow.up.left.circle"), for: .normal)
        }
        else{
            cell.formatButton.setImage(UIImage(systemName: "arrow.up.backward.and.arrow.down.forward.circle"), for: .normal)
        }
        cell.cellConfiguration(model: model)
        cell.cellIndexPath = indexPath
        cell.formatButtonTappedHandler = { [weak self] cellIndex in
            DispatchQueue.main.async {
                if let index = cellIndex{
                    self?.aux(index: index)
                }
            }
        }
        return cell
    }
    private func aux(index: IndexPath){
        tableView.scrollToRow(at: index, at: .middle, animated: false)
    }
}

extension HomeViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return isCellExpanded ? 280 : 100
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 20
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?{
        let spaceView = UIView()
        spaceView.backgroundColor = .clear
        return spaceView
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        if contentOffset < 0{
            return
        }
        if isAddMemorieButtonShown{
            if contentOffset > pastScrollValue{
                deltaY += contentOffset - pastScrollValue
            }else if deltaY > 0 {
                deltaY -= pastScrollValue - contentOffset
            }
        }else{
            if contentOffset < pastScrollValue{
                deltaY += pastScrollValue - contentOffset
            }else if deltaY > 0 {
                deltaY -= contentOffset - pastScrollValue
            }
        }
        if deltaY > deltaFlag {
            isAddMemorieButtonShown = !isAddMemorieButtonShown
            deltaY = 0
            if isAddMemorieButtonShown{
                animateAddButton()
            }else{
                animateAddButton()
            }
        }
        pastScrollValue = contentOffset
    }
}

extension HomeViewController: HomeViewModel{
    func dismissViews(completion: @escaping() -> Void){
        dismiss(animated: false, completion: completion)
    }
}





