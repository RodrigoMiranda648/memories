//
//  LateralViewController.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 17/08/23.
//

import UIKit

class LateralViewController: UIViewController {
    
    //MARK: Properties
    var viewModel: LateralViewModelImp?
    //MARK: View Components
    let containerView: UIView = {
        let view = UIView()
        view.isHidden = true
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let dismissView: UIView = {
        let view = UIView()
        
        view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private lazy var profileButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.backgroundColor = UIColor.systemBlue
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.clipsToBounds = true
        btn.layer.cornerRadius = 30
        btn.setImage(UIImage(systemName: "person.fill")?.withConfiguration(UIImage.SymbolConfiguration(pointSize: 40, weight: .regular)), for: .normal)
        btn.tintColor = UIColor.white
        btn.addTarget(self, action: #selector(profileButtonTapped), for: .touchUpInside)
        return btn
    }()
    private let stackViewVertical: UIStackView = {
        let stackView = UIStackView()
        stackView.setUpStack(axis: .vertical,
                             distribution: .equalSpacing,
                             spacing: 2)
        stackView.backgroundColor = .clear
        stackView.clipsToBounds = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    private let userNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = .clear
        let text = "User Name"
        let attributedString = NSAttributedString(string: text, attributes: [
            .font: UIFont.boldSystemFont(ofSize: 17)
        ])
        lbl.attributedText = attributedString
        lbl.clipsToBounds = true
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private let userIDLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = .systemFont(ofSize: 17)
        lbl.backgroundColor = .clear
        lbl.text = "@UserName"
        
        lbl.clipsToBounds = true
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private lazy var singOutButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.backgroundColor = UIColor.systemRed
        btn.clipsToBounds = true
        btn.layer.cornerRadius = 30
        btn.setUpTittle(title: "Cerrar Sesión",
                        fontColor: .white,
                        fontSize: 30)
        btn.setUpButton(button: btn,
                        buttonStyle: .bordered(),
                        paddingT: 10,
                        paddingL: 10,
                        paddingB: 10,
                        paddigR: 10,
                        verticalAlignment: .center,
                        horizontalAlignment: .center)
        btn.addTarget(self, action: #selector(singOutButtonTapped), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    init(viewModel: LateralViewModelImp) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        setUpViewComponents()
        setUpGestureComponents()
    }
    override func viewDidAppear(_ animated: Bool) {
        presentWithAnimation()
    }
    //MARK: Methods
    private func setUpViewComponents(){
        view.addSubview(dismissView)
        view.addSubview(containerView)
        containerView.addSubview(profileButton)
        containerView.addSubview(stackViewVertical)
        stackViewVertical.addArrangedSubview(userNameLabel)
        stackViewVertical.addArrangedSubview(userIDLabel)
        containerView.addSubview(singOutButton)
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            containerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            containerView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.7),
            
            dismissView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            dismissView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            dismissView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            dismissView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            
            profileButton.widthAnchor.constraint(equalToConstant: 60),
            profileButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            profileButton.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 40),
            profileButton.heightAnchor.constraint(equalToConstant: 60),
            
            stackViewVertical.topAnchor.constraint(equalTo: profileButton.bottomAnchor, constant: 10),
            stackViewVertical.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            stackViewVertical.heightAnchor.constraint(equalToConstant: 40),
            stackViewVertical.widthAnchor.constraint(lessThanOrEqualTo: containerView.widthAnchor, multiplier: 0.85),
            
            singOutButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            singOutButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -60),
            singOutButton.widthAnchor.constraint(equalToConstant: 150),
            singOutButton.heightAnchor.constraint(equalToConstant: 60)
        ])
    }
    private func setUpGestureComponents(){
        let tapGestureToDismiss = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        dismissView.addGestureRecognizer(tapGestureToDismiss)
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipeLeftGesture.direction = .left
        view.addGestureRecognizer(swipeLeftGesture)
    }
    func dismissWithAnimation(){
        UIView.animate(withDuration: 0.25, animations: {
            self.containerView.frame.origin.x = -self.containerView.frame.width
            self.dismissView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        }) { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
    func presentWithAnimation() {
        UIView.animate(withDuration: 0, animations: {
            self.containerView.frame.origin.x = -self.containerView.frame.width
        }) { _ in
            self.containerView.isHidden = false
            UIView.animate(withDuration: 0.25, animations: {
                self.containerView.frame.origin.x = 0
                self.dismissView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
            })
        }
    }
    //MARK: Objc functions
    @objc private func profileButtonTapped(){
        
    }
    @objc private func handleTap(_ gesture: UITapGestureRecognizer) {
        dismissWithAnimation()
    }
    @objc private func handleSwipe(){
        dismissWithAnimation()
    }
    @objc private func singOutButtonTapped(){
        viewModel?.singOut()
    }
}

extension LateralViewController: LateralViewModel{
    
}
