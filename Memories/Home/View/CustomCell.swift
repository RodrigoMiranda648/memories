//
//  CustomCell.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 27/03/23.
//

import UIKit

protocol buttonCellDelegate: AnyObject {
    func cellButtonTaped(cell: CustomCell)
}

class CustomCell: UITableViewCell {
    
    private let cellImageView: UIImageView = {
        let cell = UIImageView()
        cell.backgroundColor = UIColor.systemPink
        cell.contentMode = .scaleAspectFit
        cell.layer.borderWidth = 1
        cell.clipsToBounds = true
        cell.layer.cornerRadius = 20
        cell.translatesAutoresizingMaskIntoConstraints = false
        return cell
    }()
    
    private let descriptionLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = .systemFont(ofSize: 24)
        
        lbl.backgroundColor = .clear
        
        lbl.layer.borderWidth = 2
        lbl.clipsToBounds = true
        lbl.layer.cornerRadius = 2
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private let dateLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = .systemFont(ofSize: 15)
        lbl.backgroundColor = .clear
        lbl.clipsToBounds = true
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var formatButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.backgroundColor = .clear
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.clipsToBounds = true
        btn.setImage(UIImage(systemName: "arrow.down.right.and.arrow.up.left.circle"), for: .normal)
        btn.contentHorizontalAlignment = .fill
        btn.contentVerticalAlignment = .fill
        btn.addTarget(self, action: #selector(formatButtonTapped), for: .touchUpInside)
        return btn
    }()
    
    var imageHeight: NSLayoutConstraint?
    var imageWidth: NSLayoutConstraint?
    
    var formatButtonTappedHandler: ((IndexPath?) -> ())?
    var cellIndexPath: IndexPath?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func cellConfiguration(model: Content){
        //self.heightAnchor.constraint(equalToConstant: 370).isActive = true        sustituido por rowheight
        cellImageView.image = nil
        cellImageView.image = model.image
        descriptionLabel.text = model.description
        dateLabel.text = model.date
        self.backgroundColor = .clear
        resetConstraints()
        
        if isCellExpanded{
            self.addSubview(cellImageView)
            
            cellImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 20).isActive = true
            cellImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20).isActive = true
            imageWidth = cellImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
            imageWidth?.isActive = true
            imageHeight = cellImageView.heightAnchor.constraint(equalToConstant: 170)
            imageHeight?.isActive = true
            
            self.addSubview(descriptionLabel)
            descriptionLabel.topAnchor.constraint(equalTo: cellImageView.bottomAnchor, constant: 10).isActive = true
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
            descriptionLabel.heightAnchor.constraint(equalToConstant: 60).isActive = true
            
            self.addSubview(dateLabel)
            dateLabel.topAnchor.constraint(equalTo: self.bottomAnchor, constant: -30).isActive = true
            dateLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
            dateLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
            dateLabel.widthAnchor.constraint(equalToConstant: 80).isActive = true
            
            self.addSubview(formatButton)
            formatButton.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
            formatButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
            formatButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
            formatButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        }else{
            self.addSubview(cellImageView)
            cellImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 20).isActive = true
            cellImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
            imageWidth = cellImageView.widthAnchor.constraint(equalToConstant: 60)
            imageWidth?.isActive = true
            
            imageHeight = cellImageView.heightAnchor.constraint(equalToConstant: 60)
            imageHeight?.isActive = true
            
            self.addSubview(descriptionLabel)
            descriptionLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 20).isActive = true
            descriptionLabel.leadingAnchor.constraint(equalTo: cellImageView.trailingAnchor, constant: 10).isActive = true
            descriptionLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20).isActive = true
            descriptionLabel.heightAnchor.constraint(equalToConstant: 60).isActive = true
            
            self.addSubview(dateLabel)
            dateLabel.topAnchor.constraint(equalTo: self.bottomAnchor, constant: -30).isActive = true
            dateLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
            dateLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
            dateLabel.widthAnchor.constraint(equalToConstant: 80).isActive = true
            
            self.addSubview(formatButton)
            formatButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 30).isActive = true
            formatButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
            formatButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
            formatButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        }
    }
    
    private func resetConstraints(){
        for view in self.subviews{
            view.removeFromSuperview()
        }
        imageHeight?.isActive = false
        imageWidth?.isActive = false
    }
    
    @objc private func formatButtonTapped(){
        
      formatButtonTappedHandler?(cellIndexPath)
    }
}
