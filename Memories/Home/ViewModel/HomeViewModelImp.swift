//
//  HomeViewModelImp.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 13/04/23.
//

import UIKit

class HomeViewModelImp {
    weak var delegate: HomeViewModel?
    weak var coordinator: HomeCoordinator?
    
    //MARK: FUNCTIONS
    func openAddMemorie() {
        coordinator?.openAddMemorie()
    }
    func openLateralView(view: UIViewController){
        coordinator?.openLateralView()
    }
}

