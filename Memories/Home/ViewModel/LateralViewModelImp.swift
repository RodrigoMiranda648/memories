//
//  LateralViewModelImp.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 21/08/23.
//

import Foundation
import FirebaseAuth

class LateralViewModelImp{
    weak var delegate: LateralViewModel?
    weak var coordinator: HomeCoordinator?
    func singOut(){
        let firebaseAuth = Auth.auth()
        do {
          try firebaseAuth.signOut()
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey: "email")
            defaults.removeObject(forKey: "provider")
            coordinator?.goToRoot()
        } catch let signOutError as NSError {
          print("Error signing out: %@", signOutError)
        }
    }
}
