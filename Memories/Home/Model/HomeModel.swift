//
//  HomeModel.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 23/06/23.
//

import Foundation
import UIKit

struct Content {
   // let titte: String
    let description: String
    let image: UIImage?
    let date: String
    //let location: PlaceAnnotation
}
