//
//  AddMemorieController.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 13/04/23.
//

import UIKit


private let infoLabel = "Ingresa la información de tu recuerdo"
private let textViewPlaceHolder = "¡Cuéntale a todos lo que pasó ese día!"
private let locationLabelText = "Selecciona la ubicación en donde viviste ese recuerdo"
private let locationButtonTitle = "Selecciona una ubicación"
private let imageButtonTitle = "Selecciona una foto"

class AddMemorieController: UIViewController {
    
    private let purple = #colorLiteral(red: 0.3490196078, green: 0.1529411765, blue: 0.5843137255, alpha: 1)
    private let green = #colorLiteral(red: 0.6352941176, green: 0.7098039216, blue: 0.2039215686, alpha: 1)
    private let darkgreen = #colorLiteral(red: 0.4980392157, green: 0.6235294118, blue: 0.1607843137, alpha: 1)
    private let darkOrange = #colorLiteral(red: 0.631372549, green: 0.4745098039, blue: 0.1607843137, alpha: 1)
    private let violet = #colorLiteral(red: 0.4509803922, green: 0, blue: 0.3921568627, alpha: 1)
    
    var place: PlaceAnnotation?
    private var presentLocationController: MapViewController?
    private var viewModel: AddMemorieViewModelImp?
    private lazy var backButton: UIButton = {
        let btn = UIButton()
        
        btn.setTitle("Regresar", for: .normal)
        btn.setTitleColor(purple, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        btn.setImage(UIImage(systemName: "arrowshape.turn.up.left.fill"), for: .normal)
        btn.backgroundColor = .clear
        btn.clipsToBounds = true
        btn.contentHorizontalAlignment = .center
        btn.contentVerticalAlignment = .center
        btn.layer.cornerRadius = 15
        btn.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        
        btn.frame = CGRect(x: 0, y: 0, width: 200, height: 60)
        
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    private lazy var acceptButton: UIButton = {
        let btn = UIButton()
        
        btn.setTitle("   Publicar   ", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 25)
        // rgba(89, 39, 149, 1)
        btn.backgroundColor = purple
        btn.clipsToBounds = true
        btn.contentHorizontalAlignment = .center
        btn.contentVerticalAlignment = .center
        btn.layer.cornerRadius = 15
        btn.addTarget(self, action: #selector(acceptButtonTapped), for: .touchUpInside)
        
        btn.frame = CGRect(x: 0, y: 0, width: 200, height: 60)
        
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    private let informationLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = .systemFont(ofSize: 18)
        lbl.backgroundColor = .clear
        lbl.text = infoLabel
        lbl.clipsToBounds = true
        lbl.textAlignment = .left
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private let stackViewVertical: UIStackView = {
        let stackView = UIStackView()
        stackView.setUpStack(axis: .vertical,
                             distribution: .equalSpacing,
                             spacing: 5)
        stackView.backgroundColor = .clear
        stackView.clipsToBounds = true
        stackView.translatesAutoresizingMaskIntoConstraints = false

        return stackView
    }()
    private let titleTextField: UITextField = {
        let field = UITextField()
        
        field.font = .systemFont(ofSize: 24)
        field.attributedPlaceholder = NSAttributedString(
            string: "Título")
        field.contentVerticalAlignment = .center
        field.backgroundColor = .clear
        field.translatesAutoresizingMaskIntoConstraints = false
        
        return field
    }()
    private let contentTextView: UITextView = {
        let text = UITextView()
        
        text.font = .systemFont(ofSize: 20)
        text.backgroundColor = UIColor.init(red: 250 / 255, green: 250 / 255, blue: 250 / 255, alpha: 1)

        text.textColor = UIColor.lightGray
        text.text = textViewPlaceHolder
//        rgba(176, 176, 176, 1)
        text.layer.borderColor = CGColor.init(red: 176 / 255, green: 176 / 255, blue: 176 / 255, alpha: 0.5)
        text.layer.borderWidth = 1
        let padding: CGFloat = 2.0
        text.textContainerInset = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        
        text.layer.cornerRadius = 10
        text.clipsToBounds = true
        text.translatesAutoresizingMaskIntoConstraints = false
        
        return text
    }()
    private let stackViewElements: UIStackView = {
        let stackView = UIStackView()
        stackView.setUpStack(axis: .vertical,
                             distribution: .fillEqually,
                             spacing: 2)
        stackView.backgroundColor = .clear
        stackView.alignment = .fill
        
        stackView.layer.cornerRadius = 0
        stackView.clipsToBounds = true
        stackView.translatesAutoresizingMaskIntoConstraints = false

        return stackView
    }()
    private lazy var dateButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.backgroundColor = .clear
        btn.clipsToBounds = true

        btn.setUpButton(button: btn,
                        buttonStyle: .borderless(),
                        paddingT: 0,
                        paddingL: 2,
                        paddingB: 0,
                        paddigR: 0,
                        verticalAlignment: .center,
                        horizontalAlignment: .leading,
                        imageTitlePadding: 9)
        
        btn.setUpTittle(title: "Selecciona una fecha",
                        fontColor: UIColor.black,
                        fontSize: 18)

        btn.setUpSnippet(snippetName: "calendar",
                         color: UIColor.orange)
                
        btn.contentVerticalAlignment = .fill
        btn.contentHorizontalAlignment = .leading
        btn.addTarget(self, action: #selector(dateButtonTapped), for: .touchUpInside)
        
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    private lazy var locationButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.backgroundColor = .clear
 
        btn.setUpButton(button: btn,
                        buttonStyle: .borderless(),
                        paddingT: 0,
                        paddingL: 2,
                        paddingB: 0,
                        paddigR: 0,
                        verticalAlignment: .center,
                        horizontalAlignment: .leading,
                        imageTitlePadding: 9)
        
        btn.setUpTittle(title: locationButtonTitle,
                        fontColor: UIColor.black,
                        fontSize: 18)
        btn.setUpSnippet(snippetName: "globe.americas",
                         color: darkgreen)
        btn.setTitleColor(UIColor.black, for: .normal)
        
        //globe.americas.fill  location   airplane   map

        btn.contentVerticalAlignment = .fill
        btn.contentHorizontalAlignment = .leading
        btn.addTarget(self, action: #selector(locationButtonTapped), for: .touchUpInside)
        
        btn.layer.cornerRadius = 0
        btn.clipsToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    private lazy var imageButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.backgroundColor = .clear
 
        btn.setUpButton(button: btn,
                        buttonStyle: .borderless(),
                        paddingT: 0,
                        paddingL: 0,
                        paddingB: 0,
                        paddigR: 0,
                        verticalAlignment: .center,
                        horizontalAlignment: .leading,
                        imageTitlePadding: 5)
        
        btn.setUpTittle(title: imageButtonTitle,
                        fontColor: UIColor.black,
                        fontSize: 18)
        btn.setUpSnippet(snippetName: "camera",
                         color: violet)
        btn.setTitleColor(UIColor.black, for: .normal)
        
        //globe.americas.fill  location   airplane   map

        btn.contentVerticalAlignment = .fill
        btn.contentHorizontalAlignment = .leading
        btn.addTarget(self, action: #selector(imageButtonTapped), for: .touchUpInside)
        
        btn.layer.cornerRadius = 0
        btn.clipsToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    private let collectionViewImages: UICollectionView = {
        //Remember the instance, here is a bit different
        //Remember you must register the cell
        //Must add the datasource and asign the data source (collection.dataSource = self)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = .init(width: 120, height: 120)
        
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = .clear
        
        collection.showsHorizontalScrollIndicator = false
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    
    init(viewModel: AddMemorieViewModelImp) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetUpViewComponents()
        customizeNavBar()
        view.backgroundColor = UIColor.white
        contentTextView.delegate = self
        viewModel?.delegate = self
        collectionViewImages.dataSource = self
        collectionViewImages.register(ImageCollectionViewCell.self, forCellWithReuseIdentifier: "CollectionCell")
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func SetUpViewComponents(){
        view.addSubview(informationLabel)
        view.addSubview(stackViewVertical)
        stackViewVertical.addArrangedSubview(titleTextField)
        stackViewVertical.addArrangedSubview(contentTextView)
        
        view.addSubview(stackViewElements)
        stackViewElements.addArrangedSubview(dateButton)
        stackViewElements.addArrangedSubview(locationButton)
        stackViewElements.addArrangedSubview(imageButton)
        
        view.addSubview(collectionViewImages)
                
        NSLayoutConstraint.activate([
            
            informationLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            informationLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            informationLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            informationLabel.heightAnchor.constraint(equalToConstant: 20),
            
            stackViewVertical.topAnchor.constraint(equalTo: informationLabel.bottomAnchor, constant: 5),
            stackViewVertical.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
            stackViewVertical.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10),
            stackViewVertical.heightAnchor.constraint(equalToConstant: 400),

            
            contentTextView.heightAnchor.constraint(equalToConstant: 360),
            
            
            stackViewElements.topAnchor.constraint(equalTo: stackViewVertical.bottomAnchor, constant: 10),
            stackViewElements.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
            stackViewElements.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10),
            stackViewElements.heightAnchor.constraint(equalToConstant: 120),
            
            collectionViewImages.topAnchor.constraint(equalTo: stackViewElements.bottomAnchor, constant: 10),
            collectionViewImages.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
            collectionViewImages.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10),
            collectionViewImages.heightAnchor.constraint(equalToConstant: 120)
        ])
        

    }
    private func customizeNavBar(){
        let backButton = UIBarButtonItem(customView: backButton)
        navigationItem.leftBarButtonItem = backButton
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.tintColor = purple
        let button = UIBarButtonItem(customView: acceptButton)
        navigationItem.rightBarButtonItem = button
    }
    func updateLocation(location: PlaceAnnotation) {
        self.dismiss(animated: true)
        self.locationButton.configuration?.subtitle = location.name
    }
    
    @objc func dateButtonTapped(){
        
        viewModel?.presentDatePicker()
    }
    @objc func locationButtonTapped(){
        viewModel?.coordinator?.openMap(animated: true)
    }
    @objc func acceptButtonTapped(){
        print("ok")
    }
    @objc func imageButtonTapped(){
        viewModel?.presentImagePicker(view: self)
    }
    @objc func backButtonTapped(){
        viewModel?.coordinator?.closingHomeChildClosure?()
    }
}

extension AddMemorieController: UITextViewDelegate {
    //Adding placeholder to textview
    func textViewDidBeginEditing(_ textView: UITextView) {
        if contentTextView.text == textViewPlaceHolder {
            textView.text = ""
            textView.textColor = UIColor.black         }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {

            textView.text = textViewPlaceHolder
            textView.textColor = UIColor.lightGray
        }
    }
}

extension AddMemorieController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return memorie.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! ImageCollectionViewCell
        
        let model = memorie[indexPath.item]
        
        cell.configure(model: model)
        
        cell.backgroundColor = .clear
        
        return cell
    }
}

extension AddMemorieController: AddMemorieViewModel{
    
    func updateDate(date: String){
        self.dateButton.configuration?.subtitle = date
    }
    func setImageFromPicker(selectedImage: UIImage) {
        
        memorie.append(Content(description: "siguiente", image: selectedImage, date: "Hoy"))
        collectionViewImages.reloadData()
    }
}
