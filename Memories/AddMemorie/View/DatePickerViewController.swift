//
//  datePickerViewController.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 19/04/23.
//

import UIKit


class DatePickerViewController: UIViewController, DatePickerViewModel{
    
    var viewModel: DatePickerViewModelImp?
    
    var dateValue: String = ""
    var setDateHandler: ((_ :String) -> Void)?      //1. Aqui se define el closure
    
    lazy var datePicker: UIDatePicker = {
       let picker = UIDatePicker()

        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(dateChange(datePicker:)), for: UIControl.Event.valueChanged)
        picker.preferredDatePickerStyle = .wheels
        
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()
    
    init(viewModel: DatePickerViewModelImp) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.systemGray3
        
        setUpViewComponents()
        
        viewModel?.delegate = self
        
        guard let sheetDatePicker = presentationController as? UISheetPresentationController else{
            return
        }
        
        sheetDatePicker.detents = [.custom(resolver: { context in
            return CGFloat(250)
        })]
        //sheet.selectedDetentIdentifier =
        sheetDatePicker.prefersGrabberVisible = true
        sheetDatePicker.preferredCornerRadius = 20
    }
    
    private func setUpViewComponents(){
         
        view.addSubview(datePicker)
        
        NSLayoutConstraint.activate([
            
            datePicker.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            datePicker.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            datePicker.widthAnchor.constraint(equalToConstant: 500),
            datePicker.heightAnchor.constraint(equalToConstant: 200)
        ])
    }
    
    @objc func dateChange(datePicker: UIDatePicker){
        
        
        let dateValue = viewModel?.formatDate(date: datePicker.date)
        viewModel?.coordinator?.updateAddMemorieDateClosure?(dateValue ?? "")
    }
}

