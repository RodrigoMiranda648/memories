//
//  PlacesTableViewController.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 29/04/23.
//

import UIKit
import MapKit

class PlacesTableViewController: UIViewController {
    
    var viewModel: TableViewModelImp?
    var userLocation: CLLocation
    let places: [PlaceAnnotation]

    private let tableView: UITableView = {
        let table = UITableView()
        //table.rowHeight = CGFloat(370)
        table.allowsSelection = true
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    
    init(userLocation: CLLocation, places: [PlaceAnnotation], viewModel: TableViewModelImp) {
        self.userLocation = userLocation
        self.places = places
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        SetUpViewComponents()
        
        viewModel?.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "PlaceCell")
    }

    
    private func SetUpViewComponents(){
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 20)
        ])
    }
    
    
    private func calculateDistance(from: CLLocation, to: CLLocation) -> String{
        let feetDistance = from.distance(from: to)
        var meterDistance = feetDistance/3.2808
        var distanceString = String(format: "%.0f m", meterDistance)
        if meterDistance > 1000{
            meterDistance = meterDistance/1000
            distanceString = String(format: "%.2f Km", meterDistance)
        }
        
        return distanceString
    }
    
}

extension PlacesTableViewController: UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return places.count == 0 ? 0 : places.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCell", for: indexPath)
        let place = places[indexPath.section]
        
        var content = cell.defaultContentConfiguration()
        content.text = place.name
        content.secondaryText = calculateDistance(from: userLocation, to: place.location)
        cell.contentConfiguration = content
        
        cell.backgroundColor = place.isSelected ? UIColor.lightGray : UIColor.clear
        return cell
    }
}
    
extension PlacesTableViewController: UITableViewDelegate{
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
        let rowSelected = places[indexPath.section]
        
        viewModel?.processSelectedRow(rowSelected: rowSelected, places: self.places)
    }
    
}

extension PlacesTableViewController: TablePlacesViewModel{
    func showAlert(location: PlaceAnnotation) {
        
        let alertController = UIAlertController(title: "¿Confirmar Ubicación?", message: "¿Deseas confirmar esta ubicación: \(location.name )?", preferredStyle: .alert)
        
        let confirm = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
            
            self.viewModel?.coordinator?.updateAddMemorieLocationClosure?(location)
            self.viewModel?.coordinator?.closingAddMemorieChildClosure?()
        }
        
        let goToLocation = UIAlertAction(title: "Ver en Maps", style: .default) { (action:UIAlertAction) in
            
            location.mapItem.openInMaps()
        }
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel) { (action:UIAlertAction) in
            self.dismiss(animated: true)
        }
        
        alertController.addAction(confirm)
        alertController.addAction(goToLocation)
        alertController.addAction(cancel)
        
        self.present(alertController, animated: true)
    }
    
}



    
