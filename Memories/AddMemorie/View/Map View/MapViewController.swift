//
//  MapViewController.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 21/04/23.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController /*MapViewModel conforms in extensions*/ {
    
    var annotationFlag = 0
    var viewModel: MapViewModelImp?
    
    init(viewModel: MapViewModelImp) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let mapView: MKMapView = {
        let mp = MKMapView()
        
        mp.clipsToBounds = true
        mp.layer.cornerRadius = 20
        mp.translatesAutoresizingMaskIntoConstraints = false
        mp.showsUserLocation = true
        
        return mp
    }()
    private lazy var backButton: UIButton = {
        let btn = UIButton()
        
        btn.setUpOnlySnippetButton(snippet: "xmark.circle.fill",
                                   size: 50,
                                   weight: .bold,
                                   verticalAlignment: .center,
                                   horizontalAlignement: .center,
                                   color: UIColor.red)

        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        return btn
    }()
    lazy var searchTextfield: UITextField = {
        let field = UITextField()
        field.layer.cornerRadius = 10
        field.delegate = self
        field.clipsToBounds = true
        field.backgroundColor = UIColor.white
        field.placeholder = "Buscar"
        field.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 0))    //To add left padding
        field.leftViewMode = .always
        field.returnKeyType = .go
        field.translatesAutoresizingMaskIntoConstraints = false
        return field
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = UIColor.systemBrown
    
        viewModel?.delegate = self
        
        mapView.delegate = self
        
        viewModel?.checkLocationAutorization()
        viewModel?.initializeLocationManager()
        setUpPresentationConfiguration()
        SetUpViewComponents()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel?.coordinator?.stop()
    }
    
    private func SetUpViewComponents(){
        view.addSubview(mapView)
        view.addSubview(searchTextfield)
        view.addSubview(backButton)
        
        NSLayoutConstraint.activate([
            mapView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            backButton.topAnchor.constraint(equalTo: searchTextfield.bottomAnchor,constant: 10),
            backButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            backButton.heightAnchor.constraint(equalToConstant: 60),
            backButton.widthAnchor.constraint(equalToConstant: 60),
            
            searchTextfield.widthAnchor.constraint(equalToConstant: view.bounds.size.width/1.3),
            searchTextfield.heightAnchor.constraint(equalToConstant: 44),
            searchTextfield.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            searchTextfield.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
            
            
        ])
    }
    
    private func setUpPresentationConfiguration(){
        guard let sheetMap = presentationController as? UISheetPresentationController else{
            return
        }
        
        sheetMap.detents = [.large()]
        sheetMap.prefersGrabberVisible = true
        sheetMap.preferredCornerRadius = 20
    }
    @objc private func backButtonTapped(){
        viewModel?.coordinator?.closingAddMemorieChildClosure?()
    }
}

extension MapViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {  //Method that execute when press enter
        
        let text = textField.text ?? ""
        
        if !text.isEmpty {      //If text is no empty
            textField.resignFirstResponder()    //this method de-prioritizes the keyboard and close it
            mapView.removeAnnotations(mapView.annotations)
            
            viewModel?.requestNearbyPlaces(by: text, map: mapView)
        }
        
        
        return true
    }
}

extension MapViewController: MapViewModel {

    
    func checkLocationAutorization(region: MKCoordinateRegion) {
        mapView.setRegion(region, animated: true)
    }
    
    func showNearbyPlaces(model: [PlaceAnnotation]) {
        let places = model
        annotationFlag = 0
        places.forEach { place in
            
            if annotationFlag == 0{
                
                showLocation(location: place)
                mapView.addAnnotation(place)
            }
            
            annotationFlag += 1
            mapView.addAnnotation(place)
        }
    }
    
    func showLocation(location: PlaceAnnotation){
        let region = MKCoordinateRegion(center: location.location.coordinate, latitudinalMeters: 1200, longitudinalMeters: 1200)
        self.mapView.setRegion(region, animated: true)
    }
}

extension MapViewController : MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, didSelect annotation: MKAnnotation) {
        guard let selectedAnnotation = annotation as? PlaceAnnotation else {return}
        
        viewModel?.reaccommodateTable(selectedAnnotation: selectedAnnotation)
        viewModel?.presentTVC()
    }
}




