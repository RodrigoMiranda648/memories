//
//  AddMemorieProtocols.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 18/04/23.
//

import Foundation
import UIKit

protocol AddMemorieViewModel: AnyObject{
    func updateDate(date: String)
    func setImageFromPicker(selectedImage: UIImage)
}
