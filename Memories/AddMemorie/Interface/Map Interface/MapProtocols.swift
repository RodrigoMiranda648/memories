//
//  MapProtocols.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 30/06/23.
//

import Foundation
import MapKit

protocol MapViewModel: AnyObject{
    func checkLocationAutorization(region: MKCoordinateRegion)
    func showNearbyPlaces(model: [PlaceAnnotation])
    func showLocation(location: PlaceAnnotation)
}





