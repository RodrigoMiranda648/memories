//
//  TableProtocols.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 21/07/23.
//

import Foundation

protocol TablePlacesViewModel: AnyObject{
    func showAlert(location: PlaceAnnotation)
}
