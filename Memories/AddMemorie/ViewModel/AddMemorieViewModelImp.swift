//
//  AddMemorieModelImp.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 18/04/23.
//

import Foundation
import UIKit

//NSObject allows to inherit from UIImagePickerControllerDelegate
class AddMemorieViewModelImp: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    weak var delegate: AddMemorieViewModel?
    weak var coordinator: AddMemorieCoordinator?
    
    func presentDatePicker(){
        coordinator?.openDatePicker()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let selectedImage = info[.originalImage] as? UIImage {
            
            delegate?.setImageFromPicker(selectedImage: selectedImage)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) 
    }
    
    func presentImagePicker (view: UIViewController){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        view.present(imagePicker, animated: true, completion: nil)
    }
}

 



