//
//  DatePickerViewModelImp.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 21/07/23.

import Foundation

class DatePickerViewModelImp {
    
    weak var delegate: DatePickerViewModel?
    weak var coordinator: AddMemorieCoordinator?
    
    func formatDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        return formatter.string(from: date)
    }
    
}
