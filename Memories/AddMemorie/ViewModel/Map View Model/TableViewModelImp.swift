//
//  TableViewModelImp.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 16/07/23.
//

import Foundation
import MapKit

class TableViewModelImp {
    
    weak var delegate: TablePlacesViewModel?
    weak var coordinator: MapCoordinator?
    var places: [PlaceAnnotation]?
    
    func processSelectedRow(rowSelected : PlaceAnnotation, places : [PlaceAnnotation]) {
        
        self.places = places
        let index = rowSelected
        let selectedAnnotation = self.places!.first(where: {$0.id == index.id})
        
        self.delegate?.showAlert(location: selectedAnnotation!)
    }
}


