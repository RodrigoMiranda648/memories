//
//  MapViewModelImp.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 30/06/23.
//
import UIKit
import MapKit
import CoreLocation
 
//NSObject allows to inherit from CLLocationManagerDelegate
class MapViewModelImp: NSObject{
    
    let defaultLocation = CLLocation(latitude: 19.42847, longitude: -99.12766)
    
    weak var delegate: MapViewModel?
    weak var coordinator: MapCoordinator?
    var places: [PlaceAnnotation]?
    var tableView: PlacesTableViewController?
    
    private let locationManager = CLLocationManager()
    
    func initializeLocationManager() {
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.requestLocation()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.startUpdatingLocation()
    }
    
    func checkLocationAutorization() {
        guard let location = locationManager.location else
        { print("no se obtuvo la localización")
            return
            
        }     //Variables de uso interno de la función las cuál si no existen no se asignan, evitando errores
            
        switch locationManager.authorizationStatus {
            
        case .authorizedWhenInUse, .authorizedAlways:
            let regionToPresent = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 1200, longitudinalMeters: 1200)
            delegate?.checkLocationAutorization(region: regionToPresent)
            
        case .denied:
            print("No autorizado")
        case .authorized:
            print("Solo una vez")
            
        case .notDetermined, .restricted:
            print("Sin determinar")
        @unknown default:   //Unknown produce un warning si el elemento no coindcide con ningún caso
            print("Error desconocido, no se pudo obtener la ubicación")
        }
    }
    
    func requestNearbyPlaces(by query: String, map: MKMapView){
        //El parámetro es una codificación que se usa para quitar o dejar la codificación porcentual si el método lo acepta
           //Limpiar las anotaciones del mapa
           
           
           let request = MKLocalSearch.Request()   //Método para obtener localizaciones a partir de la busqueda
           request.naturalLanguageQuery = query    //Definir la busqueda que se va a realizar
        request.region = map.region
           
           let search = MKLocalSearch(request: request)
        search.start { response, error in   //Definición del closure
               guard let response = response, error == nil else { return } //Si existe response y no hay error, se asigna la variable response, en caso contrario no se hace nada
           
               let places = response.mapItems.map(PlaceAnnotation.init) //hace el init de cada elemento de la clase y se asigna a places
            self.places = places
            self.delegate?.showNearbyPlaces(model: self.places ?? [])
            self.presentTVC()
           }
    }
    
    func presentTVC(){
        let locationManager = locationManager
        guard let location = locationManager.location else { return }
        self.coordinator?.presentPlacesTVC(userLocation: location, places: places ?? [])
    }
    
    func reaccommodateTable(selectedAnnotation: PlaceAnnotation){
        
        let firstAnnotation = places?.first(where: {$0.id == selectedAnnotation.id})
        let index = places?.firstIndex(of: firstAnnotation!)
        let element = places!.remove(at: index!)
        places?.insert(element, at: 0)
        if let place = places?[0]{
            delegate?.showLocation(location: place)
        }
    }
}

extension MapViewModelImp: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //checkLocationAutorization()
        
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAutorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}


