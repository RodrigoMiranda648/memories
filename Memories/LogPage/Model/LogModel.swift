//
//  LogModel.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 01/08/23.
//

import Foundation

enum ProviderType: String{
    case basic
    case google
}
