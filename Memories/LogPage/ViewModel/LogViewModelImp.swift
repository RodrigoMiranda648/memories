//
//  LogViewModelImp.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 25/07/23.
//

import Foundation
import FirebaseAuth
import UIKit

class LogViewModelImp {
    weak var delegate: LogPageViewModel?
    weak var coordinator: MainCoordinator?
    var emailFunction: DispatchWorkItem?
    var passwordFunction: DispatchWorkItem?
    var validEmail = false
    var validPassword = false
    var email = ""
    var password = ""    
    
    func emailPasswordAccess(isRegister: Bool){
        //Register
        if validEmail && validPassword && isRegister{
            
            firebaseLogOut()
            
            AuthManager.shared.registerByEmail(email: email, password: password) { result in
                switch result{
                case .success(let user):
                    self.pushToHome(user: user)
                case .failure(let error):
                    if error.localizedDescription == "The email address is already in use by another account."{
                        
                        let title = "Error de registro"
                        let message = "Este email ya fue registrado anteriormente"
                        self.delegate?.showRegisterAlert(message: message, title: title)
                    }
                    else {
                        let title = "Error de registro"
                        let message = "Se produjo un error desconocido"
                        self.delegate?.showRegisterAlert(message: message, title: title)
                    }
                }
            }
        }
        //Sing In
        if validEmail && validPassword && !isRegister{
            print("iniciar sesión \(email) \(password)")
            
            Auth.auth().signIn(withEmail: email, password: password) {authResult, error in
                if error != nil {
                    print(error?.localizedDescription ?? "")
                    let title = "Error con la contraseña"
                    let message = "La contraseña es incorrecta. O se usó otro método de autenticación."
                    self.delegate?.showRegisterAlert(message: message, title: title)
                    
                    return
                }
                print(authResult?.user.email ?? "no user")
            }
        }
    }
    func googleAccess(controller: UIViewController) {
        AuthManager.shared.accessWithGoogle(controller: controller) { result in
            switch result{
                
            case .success(let user):
                self.pushToHome(user: user)
            case .failure(let error):
                let title = "Error de Ingreso con Google:"
                let message = error.localizedDescription
                self.delegate?.showRegisterAlert(message: message, title: title)
            }
        }
    }
    func facebookAccess(){
        AuthManager.shared.accesssWithFacebook { result in
            switch result{
            case .success(let user):
                self.pushToHome(user: user)
            case .failure(let error):
                if error.localizedDescription == "An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address."{
                    let title = "Error de Ingreso con Facebook:"
                    let message = "Ya existe una cuenta registrada con el mismo correo electrónico. Intenta ingresar con un método de acceso diferente."
                    self.delegate?.showRegisterAlert(message: message, title: title)
                }else{
                    let title = "Error de Ingreso con Facebook:"
                    let message = error.localizedDescription
                    self.delegate?.showRegisterAlert(message: message, title: title)
                }
            }
        }
    }
    func pushToHome(user: User){
        coordinator?.openHome(animated: true)
        print(user.providerID)
        
        let defaults = UserDefaults.standard
        defaults.set(user.email, forKey: "email")
        defaults.set(user.providerID, forKey: "provider")
    }
    func validateEmail(email: String, password: String){
        
        emailFunction?.cancel()
        //If the email has a correct format, return true
        validEmail = ValidateEmail.isValidEmail(email: email)
        if  validEmail {
            delegate?.showInvalidEmailLabel(isHidden: true)
            self.email = email
        }else if email == ""{
            delegate?.showInvalidEmailLabel(isHidden: true)
        }else{
            emailFunction = DispatchWorkItem {
                self.delegate?.showInvalidEmailLabel(isHidden: false)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: emailFunction ?? DispatchWorkItem{print("error")})
        }
        
        if password != ""{
            passwordFunction?.cancel()
            let validation = ValidatePassword.isValidPassword(password: password)
            if validation == PasswordValidation.validPassword{
                self.validPassword = true
            }
            else{
                self.validPassword = false
            }
            
            passwordFunction = DispatchWorkItem {
                switch validation {
                case .missingUppercase:
                    self.delegate?.showInvalidPasswordLabel(isHidden: false, message: "Debe contener al menos 1 mayúscula")
                case .missingLowercase:
                    self.delegate?.showInvalidPasswordLabel(isHidden: false, message: "Debe contener al menos 3 minúsculas")
                case .missingSpecial:
                    self.delegate?.showInvalidPasswordLabel(isHidden: false, message: "Debe contener al menos 1 caracter especial")
                case .missingNumber:
                    self.delegate?.showInvalidPasswordLabel(isHidden: false, message: "Debe contener al menos 1 número")
                case .validPassword:
                    self.password = password
                    self.delegate?.showInvalidPasswordLabel(isHidden: true, message: "")
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: passwordFunction ?? DispatchWorkItem{print("error")})
        }
        
        if validEmail && validPassword{
            self.delegate?.enableAccess()
        }else{
            self.delegate?.disableAccess()
        }
    }
    func firebaseLogOut(){
        let firebaseAuth = Auth.auth()
        do {
          try firebaseAuth.signOut()
        } catch let signOutError as NSError {
          print("Error signing out: %@", signOutError)
        }
    }
}


