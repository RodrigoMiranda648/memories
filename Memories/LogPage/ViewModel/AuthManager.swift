//
//  RegisterUser.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 16/08/23.
//

import Foundation
import UIKit
import FirebaseCore
import FirebaseAuth
import GoogleSignIn
import GoogleSignInSwift
import FacebookLogin

protocol Register{
    func registerByEmail(email: String, password: String, completion: @escaping (Result<User, Error>) -> Void)
    func accessWithGoogle(controller: UIViewController, completion: @escaping (Result<User, Error>) -> Void)
    func accesssWithFacebook(completion: @escaping (Result<User, Error>) -> Void)
}

class AuthManager: Register{
    let fbLoginManager = LoginManager()
    static let shared = AuthManager()
    
    func registerByEmail(email: String, password: String, completion: @escaping (Result<User, Error>) -> Void) {
        firebaseLogOut()
        Auth.auth().createUser(withEmail: email, password: password) {(result, error) in
            
            if let result = result, error == nil {
                print("registro exitoso \(result.user.email ?? "")")
                completion(.success(result.user))
                
            }
            if let error = error{
                completion(.failure(error))
            }
        }
    }
    
    func accessWithGoogle(controller: UIViewController, completion: @escaping (Result<User, Error>) -> Void) {
        GIDSignIn.sharedInstance.signOut()
        firebaseLogOut()
        guard let clientID = FirebaseApp.app()?.options.clientID else { return }
        
        // Create Google Sign In configuration object.
        let config = GIDConfiguration(clientID: clientID)
        GIDSignIn.sharedInstance.configuration = config
        
        // Start the sign in flow!
        GIDSignIn.sharedInstance.signIn(withPresenting: controller) { result, error in
            if let error = error {
                completion(.failure(error))
            }
            
            guard let user = result?.user,
                  let idToken = user.idToken?.tokenString
            else {
                if let error = error{
                    completion(.failure(error))
                }
                return
            }
            
            let credential = GoogleAuthProvider.credential(withIDToken: idToken,
                                                           accessToken: user.accessToken.tokenString)
            Auth.auth().signIn(with: credential) { result, error in

                if let error = error{
                    completion(.failure(error))
                }
                
                let user = Auth.auth().currentUser
                if let user = user {
                    completion(.success(user))
                }
            }
        }
    }
    
    func accesssWithFacebook(completion: @escaping (Result<User, Error>) -> Void) {
        fbLoginManager.logOut()
        do{
            try Auth.auth().signOut()
        } catch let signOutError as NSError {
            print("Error signing out: %@", signOutError)
        }
        firebaseLogOut()
        fbLoginManager.logIn(permissions: ["email"],
                           from: nil) { result, error in
            if let error = error{
                completion(.failure(error))
            }
            let token = result?.token?.tokenString
            let credential = FacebookAuthProvider.credential(withAccessToken: token ?? "credential")
            Auth.auth().signIn(with: credential) { authDataResult, error in
                if let error = error{
                    completion(.failure(error))
                }
                if let authDataResult = authDataResult{
                    let user = authDataResult.user
                    completion(.success(user))
                }
            }
        }
    }
    func firebaseLogOut(){
        let firebaseAuth = Auth.auth()
        do {
          try firebaseAuth.signOut()
        } catch let signOutError as NSError {
          print("Error signing out: %@", signOutError)
        }
    }
}
