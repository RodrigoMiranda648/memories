//
//  LogViewController.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 24/07/23.
//

import UIKit
import FirebaseAnalytics
import FirebaseAuth


class LogViewController: UIViewController {
        
    //MARK: Properties
    private let googleImage = UIImage(named: "google")
    private let facebookImage = UIImage(named: "facebook")
    private let twitterImage = UIImage(named: "twitter")
    private let appleImage = UIImage(named: "apple")
    var isRegisterButton = false
    var viewModel: LogViewModelImp?
    
    //MARK: View Components
    private let generalStack: UIStackView = {
        let stack = UIStackView()
        stack.setUpStack(axis: .vertical, distribution: .fill, spacing: 10)
        
        stack.backgroundColor = .clear
        
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    private let emailPasswordContentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.systemGray4
        
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let emailPasswordStack: UIStackView = {
        let stack = UIStackView()
        stack.setUpStack(axis: .vertical, distribution: .fillEqually, spacing: 10)
        
        stack.backgroundColor = .clear
        
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    private let textFieldEmail: UITextField = {
        let field = UITextField()
        
        
        field.font = .systemFont(ofSize: 18)
        field.attributedPlaceholder = NSAttributedString(
            string: "Ingresa tu email")
        field.contentVerticalAlignment = .center
        field.backgroundColor = .white
        field.keyboardType = .emailAddress
        field.autocapitalizationType = .none
        field.setLeftPaddingPoints(5)
        
        field.layer.borderColor = CGColor(red: 110 / 255, green: 110 / 255, blue: 110 / 255, alpha: 0.8)
        field.layer.borderWidth = 1
        field.layer.cornerRadius = 10
        field.clipsToBounds = true
        field.translatesAutoresizingMaskIntoConstraints = false
        return field
    }()
    private let textFieldPassword: UITextField = {
        let field = UITextField()
        
        
        field.font = .systemFont(ofSize: 18)
        field.attributedPlaceholder = NSAttributedString(
            string: "Ingresa tu contraseña")
        field.isSecureTextEntry = true
        field.contentVerticalAlignment = .center
        field.backgroundColor = .white
        field.setLeftPaddingPoints(5)
        
        field.layer.borderColor = CGColor(red: 110 / 255, green: 110 / 255, blue: 110 / 255, alpha: 0.8)
        field.layer.borderWidth = 1
        field.layer.cornerRadius = 10
        field.clipsToBounds = true
        field.translatesAutoresizingMaskIntoConstraints = false
        
        return field
    }()
    private let accessContentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.systemGray4
        
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private lazy var accessButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.backgroundColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        btn.alpha = 0.5
        
        btn.setUpButton(button: btn,
                        buttonStyle: .borderless(),
                        paddingT: 0,
                        paddingL: 0,
                        paddingB: 0,
                        paddigR: 0,
                        verticalAlignment: .center,
                        horizontalAlignment: .center,
                        imageTitlePadding: 0)
        
        btn.setUpTittle(title: "Ingresar",
                        fontColor: UIColor.white,
                        fontSize: 18)
        
        btn.addTarget(self, action: #selector(accessButtonTapped), for: .touchUpInside)
        
        btn.layer.cornerRadius = 10
        btn.clipsToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    private let validateEmailLabel: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        lbl.adjustsFontSizeToFitWidth = true
        
        lbl.text = "Ingresa un correo válido"
        lbl.textColor = .red
        
        lbl.layer.borderColor = CGColor(red: 110 / 255, green: 110 / 255, blue: 110 / 255, alpha: 0.8)
        lbl.layer.borderWidth = 1
        lbl.layer.cornerRadius = 5
        lbl.clipsToBounds = true
        lbl.isHidden = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private let validatePasswordLabel: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = .white
        lbl.adjustsFontSizeToFitWidth = true
        
        lbl.textColor = .red
        
        lbl.layer.cornerRadius = 5
        lbl.clipsToBounds = true
        lbl.isHidden = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private let registerStack: UIStackView = {
        let stack = UIStackView()
        stack.setUpStack(axis: .horizontal,
                         distribution: .fill,
                         spacing: 8)
        stack.backgroundColor = .clear
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    private let registerLabel: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = .clear
        lbl.text = "¿No tienes una cuenta? "
        lbl.textAlignment = .right
        
        
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private lazy var registerButton: UIButton = {
        let btn = UIButton(type: .custom)
        
        btn.backgroundColor = .clear
        
        btn.setUpButton(button: btn,
                        buttonStyle: .borderless(),
                        paddingT: 0,
                        paddingL: 0,
                        paddingB: 0,
                        paddigR: 0,
                        verticalAlignment: .center,
                        horizontalAlignment: .left,
                        imageTitlePadding: 0)
        
        btn.addTarget(self, action: #selector(registerButtonTapped), for: .touchUpInside)
        btn.addTarget(self, action: #selector(registerPressed), for: .touchDown)
        btn.addTarget(self, action: #selector(registerOutside), for: .touchUpOutside)
        
        btn.layer.cornerRadius = 5
        btn.clipsToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    private let registerButtonLabel: UILabel = {
        let lbl = UILabel()
        
        lbl.backgroundColor = .clear
        
        lbl.urderline(text: "Registrate")
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private let authStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setUpStack(axis: .vertical,
                             distribution: .fillEqually,
                             spacing: 20)
        stackView.backgroundColor = .clear
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    private lazy var googleButton: UIButton = {
        let btn = UIButton(type: .custom)
        
        btn.backgroundColor = UIColor.systemGray6
        
        btn.setUpButton(button: btn,
                        buttonStyle: .borderless(),
                        paddingT: 0,
                        paddingL: 0,
                        paddingB: 0,
                        paddigR: 0,
                        verticalAlignment: .center,
                        horizontalAlignment: .fill,
                        imageTitlePadding: 0)
        
        btn.addTarget(self, action: #selector(googleButtonTapped), for: .touchUpInside)
        
        btn.layer.shadowColor = UIColor.gray.cgColor
        btn.layer.shadowOffset = CGSize(width: 5, height: 5)
        btn.layer.shadowOpacity = 0.5
        btn.layer.shadowRadius = 5
        btn.layer.masksToBounds = false
        
        btn.layer.cornerRadius = 10
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    private lazy var facebookButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.backgroundColor = UIColor(red: 7 / 255, green: 53 / 255, blue: 125 / 255, alpha: 1)
        
        btn.setUpButton(button: btn,
                        buttonStyle: .borderless(),
                        paddingT: 0,
                        paddingL: 0,
                        paddingB: 0,
                        paddigR: 0,
                        verticalAlignment: .center,
                        horizontalAlignment: .fill,
                        imageTitlePadding: 0)
                        
        btn.addTarget(self, action: #selector(facebookButtonTapped), for: .touchUpInside)
        
        btn.layer.shadowColor = UIColor.gray.cgColor
        btn.layer.shadowOffset = CGSize(width: 5, height: 5)
        btn.layer.shadowOpacity = 0.5
        btn.layer.shadowRadius = 5
        btn.layer.masksToBounds = false
        
        btn.layer.cornerRadius = 10
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    init(viewModel: LogViewModelImp) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        checkDefaults()
        textFieldEmail.delegate = self
        textFieldPassword.delegate = self
        viewModel?.delegate = self
        title = "Autenticación"
        
        setUpViewComponents()
        
        setUpAuthButtonsComponents(image: googleImage!, title: "Continuar con Google", target: googleButton, textColor: .black)
        setUpAuthButtonsComponents(image: facebookImage!, title: "Continuar con Facebook", target: facebookButton, textColor: .white)
        
        view.backgroundColor = UIColor.systemGray5
        
    }
    
    //MARK: Distribution
    func setUpViewComponents(){
        
        view.addSubview(emailPasswordContentView)
        view.addSubview(generalStack)
        generalStack.addArrangedSubview(emailPasswordStack)
        
        emailPasswordStack.addArrangedSubview(textFieldEmail)
        emailPasswordStack.addArrangedSubview(textFieldPassword)
        emailPasswordStack.addArrangedSubview(accessContentView)
        accessContentView.addSubview(accessButton)
        
        view.addSubview(validateEmailLabel)
        view.addSubview(validatePasswordLabel)
        
        view.addSubview(registerStack)
        registerStack.addArrangedSubview(registerLabel)
        registerStack.addArrangedSubview(registerButton)
        registerButton.addSubview(registerButtonLabel)
        
        view.addSubview(authStackView)
        authStackView.addArrangedSubview(googleButton)
//        authStackView.addArrangedSubview(appleButton)
        authStackView.addArrangedSubview(facebookButton)
//        authStackView.addArrangedSubview(twitterButton)
        
        NSLayoutConstraint.activate([
        
            
            emailPasswordContentView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 90),
            emailPasswordContentView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
            emailPasswordContentView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10),
            emailPasswordContentView.heightAnchor.constraint(equalToConstant: 170),
            
            generalStack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 100),
            generalStack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            generalStack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            
            emailPasswordStack.heightAnchor.constraint(equalToConstant: 150),
            
            accessButton.topAnchor.constraint(equalTo: accessContentView.topAnchor),
            accessButton.bottomAnchor.constraint(equalTo: accessContentView.bottomAnchor),
            accessButton.centerXAnchor.constraint(equalTo: accessContentView.centerXAnchor),
            accessButton.widthAnchor.constraint(equalToConstant: 120),
            
            validateEmailLabel.bottomAnchor.constraint(equalTo: textFieldEmail.topAnchor, constant: -8),
            validateEmailLabel.heightAnchor.constraint(equalToConstant: 20),
            validateEmailLabel.trailingAnchor.constraint(equalTo: textFieldEmail.trailingAnchor, constant: -5),
            validateEmailLabel.widthAnchor.constraint(equalToConstant: 160),
            
            validatePasswordLabel.topAnchor.constraint(equalTo: textFieldPassword.bottomAnchor, constant: 2),
            validatePasswordLabel.heightAnchor.constraint(equalToConstant: 20),
            validatePasswordLabel.leadingAnchor.constraint(equalTo: textFieldPassword.leadingAnchor),
            validatePasswordLabel.leadingAnchor.constraint(equalTo: textFieldPassword.leadingAnchor),
            
            registerStack.topAnchor.constraint(equalTo: generalStack.bottomAnchor, constant: 10),
            registerStack.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            registerStack.widthAnchor.constraint(equalToConstant: 290),
            
            registerButton.widthAnchor.constraint(equalToConstant: 100),
            
            registerButtonLabel.topAnchor.constraint(equalTo: registerButton.topAnchor),
            registerButtonLabel.bottomAnchor.constraint(equalTo: registerButton.bottomAnchor),
            registerButtonLabel.leadingAnchor.constraint(equalTo: registerButton.leadingAnchor),
            registerButtonLabel.trailingAnchor.constraint(equalTo: registerButton.trailingAnchor),
            
            authStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -120),
            authStackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            authStackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            
            googleButton.heightAnchor.constraint(equalToConstant: 80),
            facebookButton.heightAnchor.constraint(equalToConstant: 80)
            
//            twitterButton.heightAnchor.constraint(equalToConstant: 80),
//            appleButton.heightAnchor.constraint(equalToConstant: 80)
                    
        ])
    }
    
    //MARK: Functions
    private func setUpAuthButtonsComponents(image: UIImage, title: String, target: UIButton, textColor: UIColor){
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        
        let label = UILabel()
        label.text = title
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textAlignment = .center
        label.textColor = textColor
        
        let horizontalStack: UIStackView = {
            let stack = UIStackView()
            stack.setUpStack(axis: .horizontal,
                             distribution: .fill,
                             spacing: 20)
            stack.backgroundColor = .clear
            stack.translatesAutoresizingMaskIntoConstraints = false
            stack.isUserInteractionEnabled = false
            return stack
        }()
        
        target.addSubview(horizontalStack)
        horizontalStack.addArrangedSubview(imageView)
        horizontalStack.addArrangedSubview(label)
        
        NSLayoutConstraint.activate([
            horizontalStack.topAnchor.constraint(equalTo: target.topAnchor),
            horizontalStack.bottomAnchor.constraint(equalTo: target.bottomAnchor),
            horizontalStack.leadingAnchor.constraint(equalTo: target.leadingAnchor,constant: 20),
            horizontalStack.trailingAnchor.constraint(equalTo: target.trailingAnchor,constant: -30),
            
//          ImageView's height adjust automatically with its property scaleAspectFit
            imageView.widthAnchor.constraint(equalToConstant: 40)
            
        ])
        
        
    }
    private func checkDefaults(){
        let defaults = UserDefaults.standard
        if let email = defaults.value(forKey: "email") as? String,
           let provider = defaults.value(forKey: "provider") as? String{
            viewModel?.coordinator?.openHome(animated: false)
        }
    }
    
    //MARK: Button Actions
    @objc func accessButtonTapped(){
        viewModel?.emailPasswordAccess(isRegister: isRegisterButton)
    }
    @objc func registerPressed(){
        registerButtonLabel.textColor = UIColor.systemGray
    }
    @objc func registerOutside(){
        registerButtonLabel.textColor = UIColor.black
    }
    @objc func registerButtonTapped(){
        registerButtonLabel.textColor = UIColor.black
        
        isRegisterButton = !isRegisterButton
        
        if isRegisterButton{
            registerButtonLabel.text = "Inicia Sesión"
            registerLabel.text = "¿Ya tienes una cuenta?"
            accessButton.setUpTittle(title: "Registrarse",
                                     fontColor: UIColor.white,
                                     fontSize: 18)
            accessButton.backgroundColor = #colorLiteral(red: 0.5808190107, green: 0.0884276256, blue: 0.3186392188, alpha: 1)
        }else{
            registerButtonLabel.text = "Regístrate"
            registerLabel.text = "¿No tienes una cuenta?"
            accessButton.setUpTittle(title: "Ingresar",
                                     fontColor: UIColor.white,
                                     fontSize: 18)
            accessButton.backgroundColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        }
    }
    @objc func googleButtonTapped(){
        viewModel?.googleAccess(controller: self)
    }
    @objc func facebookButtonTapped(){
        viewModel?.facebookAccess()
    }
}

//MARK: Extensions
extension LogViewController: LogPageViewModel{
    func showInvalidEmailLabel(isHidden: Bool) {
        UIView.animate(withDuration: 2) {
            self.validateEmailLabel.isHidden = isHidden
        }
    }
    func showInvalidPasswordLabel(isHidden: Bool, message: String){
        if !isHidden {
            self.validatePasswordLabel.text = message
            self.validatePasswordLabel.isHidden = isHidden
        }
        else {
            self.validatePasswordLabel.isHidden = isHidden
        }
    }
    func enableAccess(){
        self.accessButton.alpha = 1
    }
    func disableAccess(){
        self.accessButton.alpha = 0.5
    }
    func showRegisterAlert(message: String, title: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let confirm = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
            
        }
        alertController.addAction(confirm)
        self.present(alertController, animated: true)
    }
}

extension LogViewController: UITextFieldDelegate{
    func textFieldDidChangeSelection(_ textField: UITextField) {

        viewModel?.validateEmail(email: textFieldEmail.text ?? "",
                                password: textFieldPassword.text ?? "")
    }
}
