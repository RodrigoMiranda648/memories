//
//  LogPageProtocols.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 25/07/23.
//

import Foundation

protocol LogPageViewModel: AnyObject{
    func showInvalidEmailLabel(isHidden: Bool)
    func showInvalidPasswordLabel(isHidden: Bool, message: String)
    func enableAccess()
    func disableAccess()
    func showRegisterAlert(message: String, title: String)
}
