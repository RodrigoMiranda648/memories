//
//  Extensions.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 15/04/23.
//

import UIKit
import CoreLocation

extension CLLocation {      //Must import CoreLocation
    static var `default`: CLLocation {
        CLLocation(latitude: 36.063457, longitude: -95.880516)
    }
}

extension UIStackView{
    
    func setUpStack(axis: NSLayoutConstraint.Axis, distribution: UIStackView.Distribution, spacing: CGFloat){
        
        self.axis = axis
        self.distribution = distribution
    
        self.spacing = spacing
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension UIButton {
    //Set te button name to use this setUp
    //Adding this line: let btn = UIButton(type: .custom)  and giving that name to setUPConfiguration method
    
    func setUpButton(button: UIButton, buttonStyle: UIButton.Configuration, paddingT: CGFloat, paddingL: CGFloat, paddingB: CGFloat, paddigR: CGFloat, verticalAlignment: UIButton.ContentVerticalAlignment, horizontalAlignment: UIButton.ContentHorizontalAlignment, imageTitlePadding: CGFloat = 0, titleSubtitlePadding: CGFloat = 0){
        
        var config = buttonStyle
        config.imagePadding = imageTitlePadding
        config.titlePadding = titleSubtitlePadding
        config.contentInsets = NSDirectionalEdgeInsets(top: paddingT, leading: paddingL, bottom: paddingB, trailing: paddingT)
        
        button.contentVerticalAlignment = verticalAlignment
        button.contentHorizontalAlignment = horizontalAlignment
        button.configuration = config
    }
    
    func setUpTittle(title: String, fontColor: UIColor, fontSize: CGFloat){
        
        self.setTitle(title, for: .normal)
        self.setTitleColor(fontColor, for: .normal)
        self.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
    }
    
    func setUpSnippet(snippetName: String, color: UIColor){
        self.setImage(UIImage(systemName: snippetName), for: .normal)
        self.imageView?.contentMode = .scaleAspectFit
        self.tintColor = color
        
    }
    
    func setUpOnlySnippetButton(snippet: String, size: CGFloat, weight:  UIImage.SymbolWeight, verticalAlignment: UIControl.ContentVerticalAlignment, horizontalAlignement: UIControl.ContentHorizontalAlignment,color: UIColor){
        
        self.setImage(UIImage(systemName: snippet)?.withConfiguration(UIImage.SymbolConfiguration(pointSize: size, weight: weight)), for: .normal)
        self.contentVerticalAlignment = verticalAlignment
        self.contentHorizontalAlignment = horizontalAlignement
        self.tintColor = color
    }
}

extension UILabel {
    
    func urderline(text: String){
        let labelText = text
        let attributedString = NSMutableAttributedString(string: labelText)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length))
        self.attributedText = attributedString
    }
}
