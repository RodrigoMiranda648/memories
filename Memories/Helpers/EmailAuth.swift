//
//  EmailAuth.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 26/07/23.
//

import Foundation

enum PasswordValidation{
    case missingUppercase
    case missingLowercase
    case missingSpecial
    case missingNumber
    case validPassword
}

final class ValidateEmail {
    static func isValidEmail(email: String) -> Bool {
        // email structure
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        //Function's argument will be compared with emailRegex
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        // result with true or false
        return emailPredicate.evaluate(with: email)
    }
}

final class ValidatePassword{
    static func isValidPassword(password: String) -> PasswordValidation{
        // At least 2 Uppercase, 3 Lowercase, 1 Especial Character and 1 number
        let validation = ["^(.*[A-Z].*){1,}$", "^(.*[a-z].*){3,}$","^(.*[^A-Z-a-z09].*){1,}$","^(.*[0-9].*){1,}$"]
        for item in 0..<validation.count{
            let evaluation = NSPredicate(format: "SELF MATCHES %@", validation[item])
            let isValid = evaluation.evaluate(with: password)
            if item == 0 && !isValid {
                return PasswordValidation.missingUppercase
            }
            if item == 1 && !isValid {
                return PasswordValidation.missingLowercase
            }
            if item == 2 && !isValid {
                return PasswordValidation.missingSpecial
            }
            if item == 3 && !isValid {
                return PasswordValidation.missingNumber
            }
        }
        return PasswordValidation.validPassword
    }
}
