//
//  HomeCoordinator.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 30/08/23.
//

import Foundation
import UIKit

class HomeCoordinator: Coordinator{
    
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []
    var closingMainChildClosure: (() -> Void)?
    
    init(rootViewController: UINavigationController) {
        self.navigationController = rootViewController
    }
    func start(animated: Bool) {
        let viewModel = HomeViewModelImp()
        viewModel.coordinator = self
        let homeViewController = HomeViewController(viewModel: viewModel)
        navigationController.pushViewController(homeViewController, animated: animated)
    }
    func openAddMemorie(){
        let addMemorieCoordinator = AddMemorieCoordinator(rootViewController: navigationController)
        addMemorieCoordinator.start(animated: true)
        childCoordinators.append(addMemorieCoordinator)
        addMemorieCoordinator.closingHomeChildClosure = {
            self.navigationController.dismiss(animated: false)
            self.navigationController.popViewController(animated: true)
            self.childCoordinators.removeAll()
        }
    }
    func openLateralView(){
        let viewModel = LateralViewModelImp()
        viewModel.coordinator = self
        let lateral = LateralViewController(viewModel: viewModel)
        lateral.modalPresentationStyle = .overCurrentContext
        navigationController.present(lateral, animated: false, completion: nil)
    }
    func goToRoot(){
        closingMainChildClosure?()
    }
}
