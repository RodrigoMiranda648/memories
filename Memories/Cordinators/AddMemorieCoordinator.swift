//
//  AddMemorieCoordinator.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 30/08/23.
//

import Foundation
import UIKit

class AddMemorieCoordinator: NSObject, Coordinator {
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []
    var closingHomeChildClosure: (() -> Void)?
    var updateAddMemorieDateClosure: ((String) -> Void)?
    var updateAddMemorieLocationClosure: ((PlaceAnnotation) -> Void)?
    
    init(rootViewController: UINavigationController) {
        self.navigationController = rootViewController
    }
    
    func start(animated: Bool) {
        let viewModel = AddMemorieViewModelImp()
        viewModel.coordinator = self
        let addMemorie = AddMemorieController(viewModel: viewModel)
        updateAddMemorieDateClosure = { [] newDate in
            addMemorie.updateDate(date: newDate)
        }
        updateAddMemorieLocationClosure = { [] newLocation in
            addMemorie.updateLocation(location: newLocation)
        }
        navigationController.pushViewController(addMemorie, animated: animated)
    }
    func openDatePicker(){
        let viewModel = DatePickerViewModelImp()
        viewModel.coordinator = self
        let picker = DatePickerViewController(viewModel: viewModel)
        navigationController.present(picker, animated: true, completion: nil)
    }
    func openMap(animated: Bool){
        let mapCoordinator = MapCoordinator(rootViewController: navigationController)
        mapCoordinator.start(animated: animated)
        childCoordinators.append(mapCoordinator)
        mapCoordinator.updateAddMemorieLocationClosure = { [] newLocation in
            self.updateAddMemorieLocationClosure?(newLocation)
        }
        mapCoordinator.closingAddMemorieChildClosure = {
            self.navigationController.dismiss(animated: false)
            self.navigationController.popViewController(animated: true)
            self.childCoordinators.removeAll()
        }
    }
    func openPhotoPicker(){
        
    }
    func stop(){
        closingHomeChildClosure?()
    }
}


