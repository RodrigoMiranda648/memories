//
//  MainCoordinator.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 28/08/23.
//

import Foundation
import UIKit

protocol Coordinator: AnyObject {
    var navigationController: UINavigationController {get}
    var childCoordinators: [Coordinator] {get}
    func start(animated: Bool)
}

class MainCoordinator: Coordinator{
    var childCoordinators: [Coordinator] = []
    var navigationController = UINavigationController()

    func start(animated: Bool) {
        let viewModel = LogViewModelImp()
        viewModel.coordinator = self
        let vc = LogViewController(viewModel: viewModel)
        navigationController.pushViewController(vc, animated: false)
    }
    func openHome(animated: Bool){
        let homeCoordinator = HomeCoordinator(rootViewController: navigationController)
        homeCoordinator.start(animated: animated)
        childCoordinators.append(homeCoordinator)
        homeCoordinator.closingMainChildClosure = {
            self.navigationController.dismiss(animated: false)
            self.navigationController.popToRootViewController(animated: true)
            _ = self.childCoordinators.popLast()
        }
    }
}










