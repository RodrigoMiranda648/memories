//
//  MapCoordinator.swift
//  Memories
//
//  Created by Rodrigo Miranda Castillo on 30/08/23.
//

import Foundation
import UIKit
import CoreLocation

class MapCoordinator: Coordinator{
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []
    var closingAddMemorieChildClosure: (() -> Void)?
    var updateAddMemorieLocationClosure: ((PlaceAnnotation) -> Void)?
    
    init(rootViewController: UINavigationController) {
        self.navigationController = rootViewController
    }
    
    func start(animated: Bool) {
        let viewModel = MapViewModelImp()
        viewModel.coordinator = self
        let mapView = MapViewController(viewModel: viewModel)
        navigationController.pushViewController(mapView, animated: true)
    }
    func presentPlacesTVC(userLocation: CLLocation, places: [PlaceAnnotation]){
        
        let viewModel = TableViewModelImp()
        viewModel.coordinator = self
        let tableView = PlacesTableViewController(userLocation: userLocation, places: places,viewModel: viewModel)
        if let sheet = tableView.sheetPresentationController {
            sheet.detents = [.custom(resolver: { context in
                CGFloat(300)
            })]
            sheet.prefersGrabberVisible = true
            sheet.preferredCornerRadius = 20
            
            navigationController.present(tableView, animated: true)
        }
    }
    func stop(){
        closingAddMemorieChildClosure?()
    }
}
